
Tentang aplikasi :
~ Aplikasi membutuhkan internet untuk mengakses dan menampilkan gambar
~ Sumber deskripsi untuk setiap versi - versi Windows berasal dari https://en.wikipedia.org/


Referensi code:

// splash screen :
- https://www.codepolitan.com/mudah-membuat-splash-screen-dengan-android-studio-5a8d2310894d1
    ** aplikasi ini menggunakan layout activity sementara pada web hanya menggunakan xml pada folder drawable

// recycler view dan actionlistener dst:
- https://www.youtube.com/watch?v=ZXoGG2XTjzU

// slider gambar pada DetailACtivity.kt :
- https://stackoverflow.com/questions/43148848/how-to-load-images-with-glide-dynamically-android
    ** menggunakan tanggapan dari user Faisal (tanggapan berada di paling bawah saat tanggal 12 Septermber 2019)
