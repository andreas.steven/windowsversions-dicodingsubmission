/**
 * Class untuk menampilkan menu awal
 */

package com.dicodingSubmisssion.windowsversions

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    // Deklarasi dan inisiaisasi variabel
    private lateinit var rvWindows: RecyclerView
    private var listOfWindows: ArrayList<Windows> = arrayListOf()
    private var title: String = "Home"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (supportActionBar as ActionBar).title = this.title

        rvWindows = findViewById(R.id.listOfItems)
        rvWindows.setHasFixedSize(true)
        listOfWindows.addAll(WindowsData.listOfDetails)
        showList()
    }

    // Memanggil data dari WindowsData.kt ke adapter
    private fun showList(){
        rvWindows.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        val winAdapter = WindowsAdapter(listOfWindows)
        rvWindows.adapter = winAdapter
    }


    // Fungsi - fungsi untuk set isi menu "About Me" agar bisa ditampilkan dan diklik
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.about_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        goTo(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    private fun goTo(selectedMenu: Int){
        when (selectedMenu){
            R.id.intent_about -> {
                var gotoActivity = Intent(this@MainActivity, AboutActivity::class.java)
                startActivity(gotoActivity)
            }
        }
    }
}
