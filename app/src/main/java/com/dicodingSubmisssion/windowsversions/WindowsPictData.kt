/**
 * Class yang berisi data gambar - gambar untuk setiap versi-versi Windows
 */

package com.dicodingSubmisssion.windowsversions

object WindowsPictData {

    private val pictures = arrayOf(
        /**
         * gambar Windows 1.0
         */
        arrayOf(
            "https://cdn.wccftech.com/wp-content/uploads/2019/07/windows-1.0.jpg",
            "https://upload.wikimedia.org/wikipedia/en/4/4e/Windows1.0.png",
            "https://upload.wikimedia.org/wikipedia/commons/d/df/Microsoft_Windows_1.0_screenshot.png",
            "https://upload.wikimedia.org/wikipedia/en/7/71/Microsoft_Windows_1.01_multitasking.png",
            "https://upload.wikimedia.org/wikipedia/en/4/47/MS-DOS_Executive%2C_Windows_1.04.png"
        ),

        /**
         * gambar Windows 2.x
         */
        arrayOf(
            "https://winworldpc.com/res/img/screenshots/da0a6cda7c563cbb4033b8910d61e567ed293af2595355c543c473569c2c70c3.png",
            "https://upload.wikimedia.org/wikipedia/en/c/c9/Windows_2.0.png",
            "https://vignette.wikia.nocookie.net/windows/images/5/5a/Windows_2.0_desktop-0/revision/latest?cb=20190601165043",
            "https://upload.wikimedia.org/wikipedia/en/8/8f/Windows_2.1.png",
            "https://gonnagan.files.wordpress.com/2011/07/win200_217.png",
            "https://winworldpc.com/res/img/screenshots/20-for-windows-6eaef8756c67f2ce78053665857de36c-WordStar%20for%20Windows%202.0%20-%20Splash.png"
        ),

        /**
         * gambar Windows 3.x
         */
        arrayOf(
            "https://upload.wikimedia.org/wikipedia/en/1/15/Windows_3.0_workspace.png",
            "https://upload.wikimedia.org/wikipedia/en/7/73/Windows_3.11_workspace.png",
            "https://upload.wikimedia.org/wikipedia/en/6/64/Windows_3.1.png",
            "https://upload.wikimedia.org/wikipedia/en/c/c2/Windows_for_Workgroups.png"
        ),

        /**
         * gambar Windows 9x dan ME
         */
        arrayOf(
            "https://upload.wikimedia.org/wikipedia/en/9/91/Windows_95_Desktop_screenshot.png",
            "https://upload.wikimedia.org/wikipedia/en/e/eb/Windows_95_at_first_run.png",
            "https://upload.wikimedia.org/wikipedia/en/0/00/Windows98.png",
            "https://upload.wikimedia.org/wikipedia/en/6/69/WindowsME.png"
        ),

        /**
         * gambar Windows 2000
         */
        arrayOf(
            "https://upload.wikimedia.org/wikipedia/en/c/c6/Windows_2000_Server.png",
            "https://upload.wikimedia.org/wikipedia/en/8/8c/Windows_2000_Explorer.png",
            "https://upload.wikimedia.org/wikipedia/en/6/69/Win2kDefrag.png",
            "https://upload.wikimedia.org/wikipedia/commons/5/5a/Windows_2000_Recovery_Console.png"
        ),

        /**
         * gambar Windows XP
         */
        arrayOf(
            "https://upload.wikimedia.org/wikipedia/en/6/64/Windows_XP_Luna.png",
            "https://upload.wikimedia.org/wikipedia/en/a/a5/RoyaleXP2.PNG",
            "https://upload.wikimedia.org/wikipedia/en/f/f3/Windows_Security_Center_XP_SP2.png"
        ),

        /**
         * gambar Windows Vista
         */
        arrayOf(
            "https://upload.wikimedia.org/wikipedia/en/a/a3/Windows_Vista.png",
            "https://upload.wikimedia.org/wikipedia/en/a/a8/Microsoft_Codename_LongHorn_v6.0_Build_4074.png",
            "https://upload.wikimedia.org/wikipedia/en/6/6d/Vista-visual-styles-comparison.png"
        ),

        /**
         * gambar Windows 7
         */
        arrayOf(
            "https://upload.wikimedia.org/wikipedia/en/5/50/Windows_7_SP1_screenshot.png",
            "https://upload.wikimedia.org/wikipedia/en/2/25/ThumbnailWin7.png",
            "https://upload.wikimedia.org/wikipedia/en/7/72/Action_Center_on_Windows_7.png",
            "https://upload.wikimedia.org/wikipedia/en/d/d6/Action-centre-warning.PNG"
        ),

        /**
         * gambar Windows 8.0 dan 8.1
         */
        arrayOf(
            "https://upload.wikimedia.org/wikipedia/en/8/8e/Windows_8_Start_Screen.png",
            "https://upload.wikimedia.org/wikipedia/en/5/53/Windows8102MultiMonitorAndApp.png",
            "https://upload.wikimedia.org/wikipedia/commons/2/2c/Windows_8_-_Xbox_Music_and_Photos_together.png",
            "https://upload.wikimedia.org/wikipedia/commons/c/c5/Wikipedia_App_snapped_to_Windows_8_desktop.png"
        ),

        /**
         * gambar Windows 10
         */
        arrayOf(
            "https://upload.wikimedia.org/wikipedia/en/5/51/Windows_10_1903_Desktop.png",
            "https://upload.wikimedia.org/wikipedia/en/1/1f/Virtual_Desktops_in_Windows_10.png",
            "https://upload.wikimedia.org/wikipedia/en/0/09/Windows_Hello_in_Enpass.png"
        )
    )
    val listOfPictures: ArrayList< ArrayList<String> >
        get() {

            val retList = ArrayList< ArrayList<String> >()
            for (i in 0 until pictures.size) {
                val data = ArrayList<String> ()

                for (j in 0 until pictures[i].size) {
                    data.add(pictures[i][j])
                }

                retList.add(data)
            }

            return retList
        }
}