/**
 * Class yang berisi data detil tiap versi - versi Windows
 */

package com.dicodingSubmisssion.windowsversions

object WindowsData {

    private val datas = arrayOf(
        
        /**
         * Windows 1.0
         */
        arrayOf(
            // group, name, version, codename, relDate, desc, logo, dev
            "Windows",
            "Windows 1.0",
            "1.0",
            "Interface Manager",
            "November 1985",
            "       Windows 1.0 was announced in November 1983 (after the Apple Lisa, but before the Macintosh) under the name \"Windows\", but Windows 1.0 was not released until November 1985. Windows 1.0 was to compete with Apple's operating system, but achieved little popularity. Windows 1.0 is not a complete operating system; rather, it extends MS-DOS. The shell of Windows 1.0 is a program known as the MS-DOS Executive. Components included Calculator, Calendar, Cardfile, Clipboard viewer, Clock, Control Panel, Notepad, Paint, Reversi, Terminal and Write. Windows 1.0 does not allow overlapping windows. Instead all windows are tiled. Only modal dialog boxes may appear over other windows. Microsoft sold as included Windows Development libraries with the C development environment, which included numerous windows samples.",
            "https://png2.cleanpng.com/sh/a33c0a774d3617a643ab5ed497303af5/L0KzQYm3VsE0N6N3jJH0aYP2gLBuTgdqdpV0jAU2MT2zPcjwjvRwf6QyUac2d3nxdLFAk706QF5phAU2d3nxdLFAk70yOF5xh9l4LUXlQoHshvE5a5doTaUBLkG1SIKBU8UxOWY3UKoDNUG5QIm8Ub5xdpg=/kisspng-windows-1-0-windows-95-windows-98-dos-windows-10-logo-5b20efa8cfc536.128183501528885160851.png",
            "Microsoft"
        ),

        /**
         * Windows 2.x
         */
        arrayOf(
            // group, name, version, codename, relDate, desc, logo, dev
            "Windows",
            "Windows 2.x",
            "2.x",
            "-",
            "December 1987",
            "       Windows 2.0 was released in December 1987, and was more popular than its predecessor. It features several improvements to the user interface and memory management. Windows 2.03 changed the OS from tiled windows to overlapping windows. The result of this change led to Apple Computer filing a suit against Microsoft alleging infringement on Apple's copyrights. Windows 2.0 also introduced more sophisticated keyboard shortcuts and could make use of expanded memory \n."+
                "\n" +
                "       Windows 2.1 was released in two different versions: Windows/286 and Windows/386. Windows/386 uses the virtual 8086 mode of the Intel 80386 to multitask several DOS programs and the paged memory model to emulate expanded memory using available extended memory. Windows/286, in spite of its name, runs on both Intel 8086 and Intel 80286 processors. It runs in real mode but can make use of the high memory area.\n" +
                "\n" +
                "       In addition to full Windows-packages, there were runtime-only versions that shipped with early Windows software from third parties and made it possible to run their Windows software on MS-DOS and without the full Windows feature set.",
            "https://png2.cleanpng.com/sh/a33c0a774d3617a643ab5ed497303af5/L0KzQYm3VsE0N6N3jJH0aYP2gLBuTgdqdpV0jAU2MT2zPcjwjvRwf6QyUac2d3nxdLFAk706QF5phAU2d3nxdLFAk70yOF5xh9l4LUXlQoHshvE5a5doTaUBLkG1SIKBU8UxOWY3UKoDNUG5QIm8Ub5xdpg=/kisspng-windows-1-0-windows-95-windows-98-dos-windows-10-logo-5b20efa8cfc536.128183501528885160851.png",
            "Microsoft"
        ),

        /**
         * Windows 3.x
         */
        arrayOf(
            // group, name, version, codename, relDate, desc, logo, dev
            "Windows",
            "Windows 3.x",
            "3.x",
            "Sparta, Winball, Snowball",
            "1990 (3.0) ",
            "       Windows 3.0, released in 1990, improved the design, mostly because of virtual memory and loadable virtual device drivers (VxDs) that allow Windows to share arbitrary devices between multi-tasked DOS applications.[citation needed] Windows 3.0 applications can run in protected mode, which gives them access to several megabytes of memory without the obligation to participate in the software virtual memory scheme. They run inside the same address space, where the segmented memory provides a degree of protection. Windows 3.0 also featured improvements to the user interface. Microsoft rewrote critical operations from C into assembly. Windows 3.0 is the first Microsoft Windows version to achieve broad commercial success, selling 2 million copies in the first six months.\n" +
                "\n" +
                "       Windows 3.1, made generally available on March 1, 1992, featured a facelift. In August 1993, Windows for Workgroups, a special version with integrated peer-to-peer networking features and a version number of 3.11, was released. It was sold along with Windows 3.1. Support for Windows 3.1 ended on December 31, 2001.\n" +
                "\n" +
                "       Windows 3.2, released 1994, is an updated version of the Chinese version of Windows 3.1. The update was limited to this language version, as it fixed only issues related to the complex writing system of the Chinese language. Windows 3.2 was generally sold by computer manufacturers with a ten-disk version of MS-DOS that also had Simplified Chinese characters in basic output and some translated utilities.",
            "https://png2.cleanpng.com/sh/a33c0a774d3617a643ab5ed497303af5/L0KzQYm3VsE0N6N3jJH0aYP2gLBuTgdqdpV0jAU2MT2zPcjwjvRwf6QyUac2d3nxdLFAk706QF5phAU2d3nxdLFAk70yOF5xh9l4LUXlQoHshvE5a5doTaUBLkG1SIKBU8UxOWY3UKoDNUG5QIm8Ub5xdpg=/kisspng-windows-1-0-windows-95-windows-98-dos-windows-10-logo-5b20efa8cfc536.128183501528885160851.png",
            "Microsoft"
        ),

        /**
         * Windows 9x and ME
         */
        arrayOf(
            // group, name, version, codename, relDate, desc, logo, dev
            "Windows",
            "Windows 9x, Windows ME",
            "9x, ME",
            "Chicago (95), Memphis (98), Millenium (ME)",
            "24 August 1995",
            "       Windows 95, was released on August 24, 1995. While still remaining MS-DOS-based, Windows 95 introduced support for native 32-bit applications, plug and play hardware, preemptive multitasking, long file names of up to 255 characters, and provided increased stability over its predecessors. Windows 95 also introduced a redesigned, object oriented user interface, replacing the previous Program Manager with the Start menu, taskbar, and Windows Explorer shell. Windows 95 was a major commercial success for Microsoft; Ina Fried of CNET remarked that \"by the time Windows 95 was finally ushered off the market in 2001, it had become a fixture on computer desktops around the world.\" Microsoft published four OEM Service Releases (OSR) of Windows 95, each of which was roughly equivalent to a service pack. The first OSR of Windows 95 was also the first version of Windows to be bundled with Microsoft's web browser, Internet Explorer.[21] Mainstream support for Windows 95 ended on December 31, 2000, and extended support for Windows 95 ended on December 31, 2001.\n" +
                "\n" +
                "       Windows 95 was followed up with the release of Windows 98 on June 25, 1998, which introduced the Windows Driver Model, support for USB composite devices, support for ACPI, hibernation, and support for multi-monitor configurations. Windows 98 also included integration with Internet Explorer 4 through Active Desktop and other aspects of the Windows Desktop Update (a series of enhancements to the Explorer shell which were also made available for Windows 95). In May 1999, Microsoft released Windows 98 Second Edition, an updated version of Windows 98. Windows 98 SE added Internet Explorer 5.0 and Windows Media Player 6.2 amongst other upgrades. Mainstream support for Windows 98 ended on June 30, 2002, and extended support for Windows 98 ended on July 11, 2006.\n" +
                "\n" +
                "       On September 14, 2000, Microsoft released Windows Me (Millennium Edition), the last DOS-based version of Windows. Windows Me incorporated visual interface enhancements from its Windows NT-based counterpart Windows 2000, had faster boot times than previous versions (which however, required the removal of the ability to access a real mode DOS environment, removing compatibility with some older programs),expanded multimedia functionality (including Windows Media Player 7, Windows Movie Maker, and the Windows Image Acquisition framework for retrieving images from scanners and digital cameras), additional system utilities such as System File Protection and System Restore, and updated home networking tools. However, Windows Me was faced with criticism for its speed and instability, along with hardware compatibility issues and its removal of real mode DOS support. PC World considered Windows Me to be one of the worst operating systems Microsoft had ever released, and the 4th worst tech product of all time.",
            "https://www.pinclipart.com/picdir/middle/7-70537_microsoft-windows-clipart-transparent-windows-95-logo-png.png",
            "Microsoft"
        ),

        /**
         * Windows 2000
         */
        arrayOf(
            // group, name, version, codename, relDate, desc, logo, dev
            "Windows",
            "Windows 2000",
            "2000 (NT 5.0)",
            "Janus",
            "15 December 1999, 17 February 2000 (Retail)",
            "       Windows 2000 is an operating system that was produced by Microsoft as part of the Windows NT family of operating systems. It was released to manufacturing on December 15, 1999, and launched to retail on February 17, 2000. It is the successor to Windows NT 4.0.\n" +
                "\n" +
                "       Four editions of Windows 2000 were released: Professional, Server, Advanced Server, and Datacenter Server; the latter was both released to manufacturing and launched months after the other editions. While each edition of Windows 2000 was targeted at a different market, they shared a core set of features, including many system utilities such as the Microsoft Management Console and standard system administration applications.",
            "https://www.pinclipart.com/picdir/middle/7-70537_microsoft-windows-clipart-transparent-windows-95-logo-png.png",
            "Microsoft"
        ),

        /**
         * Windows XP
         */
        arrayOf(
            // group, name, version, codename, relDate, desc, logo, dev
            "Windows",
            "Windows XP",
            "XP",
            "Whistler",
            "24 August 2001",
            "       Windows XP is a personal computer operating system produced by Microsoft as part of the Windows NT family of operating systems. It was released to manufacturing on August 24, 2001, and broadly released for retail sale on October 25, 2001.\n" +
                "\n" +
                "       Development of Windows XP began in the late 1990s as \"Neptune\", an operating system (OS) built on the Windows NT kernel which was intended specifically for mainstream consumer use. An updated version of Windows 2000 was also originally planned for the business market; however, in January 2000, both projects were scrapped in favor of a single OS codenamed \"Whistler\", which would serve as a single OS platform for both consumer and business markets. As such, Windows XP was the first consumer edition of Windows not to be based on MS-DOS.[5]\n" +
                "\n" +
                "       Upon its release, Windows XP received critical acclaim, with critics noting increased performance and stability (especially in comparison to Windows Me, the previous version of Windows aimed at home users), a more intuitive user interface, improved hardware support, and expanded multimedia capabilities. However, some industry reviewers were concerned by the new licensing model and product activation system.\n" +
                "\n" +
                "       Extended support for Windows XP ended on April 8, 2014, after which the operating system ceased receiving further support or security updates (with exceptional security updates being made e.g. in 2019, to address potential ransomware threats) to most users. By August 2019, Microsoft (and others) had ended support for games on Windows XP. As of August 2019, 1.65% of Windows PCs run Windows XP.[7] At least one country has double digit use, Armenia where it's highest ranked at 44.6%,[8] and China is also exceptionally high at 5.38%.",
            "https://upload.wikimedia.org/wikipedia/en/thumb/e/e4/Windows_logo_-_2002.svg/173px-Windows_logo_-_2002.svg.png",
            "Microsoft"
        ),

        /**
         * Windows Vista
         */
        arrayOf(
            // group, name, version, codename, relDate, desc, logo, dev
            "Windows",
            "Windows Vista",
            "Vista",
            "Longhorn",
            "30 January 2007",
            "       Windows Vista is an operating system that was produced by Microsoft for use on personal computers, including home and business desktops, laptops, tablet PCs and media center PCs. Development was completed on November 8, 2006, and over the following three months, it was released in stages to computer hardware and software manufacturers, business customers and retail channels. On January 30, 2007, it was released worldwide and was made available for purchase and download from the Windows Marketplace; it is the first release of Windows to be made available through a digital distribution platform. The release of Windows Vista came more than five years after the introduction of its predecessor, Windows XP, the longest time span between successive releases of Microsoft Windows desktop operating systems.\n" +
                "\n" +
                "       New features of Windows Vista include an updated graphical user interface and visual style dubbed Aero, a new search component called Windows Search, redesigned networking, audio, print and display sub-systems, and new multimedia tools such as Windows DVD Maker. Vista aimed to increase the level of communication between machines on a home network, using peer-to-peer technology to simplify sharing files and media between computers and devices. Windows Vista included version 3.0 of the .NET Framework, allowing software developers to write applications without traditional Windows APIs.\n" +
                "\n" +
                "       Microsoft's primary stated objective with Windows Vista was to improve the state of security in the Windows operating system. One common criticism of Windows XP and its predecessors was their commonly exploited security vulnerabilities and overall susceptibility to malware, viruses and buffer overflows. In light of this, Microsoft chairman Bill Gates announced in early 2002 a company-wide \"Trustworthy Computing initiative\", which aimed to incorporate security into every aspect of software development at the company. Microsoft stated that it prioritized improving the security of Windows XP and Windows Server 2003 above finishing Windows Vista, thus delaying its completion.\n" +
                "\n" +
                "       While these new features and security improvements have garnered positive reviews, Vista has also been the target of much criticism and negative press. Criticism of Windows Vista has targeted its high system requirements, its more restrictive licensing terms, the inclusion of a number of then-new DRM technologies aimed at restricting the copying of protected digital media, lack of compatibility with some pre-Vista hardware and software, longer boot time, and the number of authorization prompts for User Account Control. As a result of these and other issues, Windows Vista had seen initial adoption and satisfaction rates lower than Windows XP. However, with an estimated 330 million Internet users as of January 2009, it had been announced that Vista usage had surpassed Microsoft's pre-launch two-year-out expectations of achieving 200 million users.",
            "https://upload.wikimedia.org/wikipedia/en/thumb/1/14/Windows_logo_-_2006.svg/335px-Windows_logo_-_2006.svg.png",
            "Microsoft"
        ),

        /**
         * Windows 7
         */
        arrayOf(
            // group, name, version, codename, relDate, desc, logo, dev
            "Windows",
            "Windows 7",
            "7",
            "Blackcomb (before Vienna), Vienna",
            "22 July 2009",
            "       Windows 7 is a personal computer operating system that was produced by Microsoft as part of the Windows NT family of operating systems. It was released to manufacturing on July 22, 2009 and became generally available on October 22, 2009, less than three years after the release of its predecessor, Windows Vista. Windows 7's server counterpart, Windows Server 2008 R2, was released at the same time.\n" +
                "\n" +
                "       Windows 7 was primarily intended to be an incremental upgrade to Microsoft Windows, intended to address Windows Vista's poor critical reception while maintaining hardware and software compatibility. Windows 7 continued improvements on Windows Aero (the user interface introduced in Windows Vista) with the addition of a redesigned taskbar that allows applications to be \"pinned\" to it, and new window management features. Other new features were added to the operating system, including libraries, the new file sharing system HomeGroup, and support for multitouch input. A new \"Action Center\" interface was also added to provide an overview of system security and maintenance information, and tweaks were made to the User Account Control system to make it less intrusive. Windows 7 also shipped with updated versions of several stock applications, including Internet Explorer 8, Windows Media Player, and Windows Media Center.\n" +
                "\n" +
                "       In contrast to Windows Vista, Windows 7 was generally praised by critics, who considered the operating system to be a major improvement over its predecessor due to its increased performance, its more intuitive interface (with particular praise devoted to the new taskbar), fewer User Account Control popups, and other improvements made across the platform. Windows 7 was a major success for Microsoft; even prior to its official release, pre-order sales for 7 on the online retailer Amazon.com had surpassed previous records. In just six months, over 100 million copies had been sold worldwide, increasing to over 630 million licenses by July 2012. As of July 2019, 31.24% of computers running Windows are running Windows 7, which still has over 50% market share in a number of countries such as in China at 51.21%, and is the most used version in many countries, mostly those in Africa. Windows 10 is by now most popular on all continents, after overtaking Windows 7 in Africa.",
            "https://upload.wikimedia.org/wikipedia/en/thumb/1/14/Windows_logo_-_2006.svg/335px-Windows_logo_-_2006.svg.png",
            "Microsoft"
        ),

        /**
         * Windows 8.0, and 8.1
         */
        arrayOf(
            // group, name, version, codename, relDate, desc, logo, dev
            "Windows",
            "Windows 8.0, Windows 8.1",
            "8.0, 8.1",
            "Blue",
            "26 October 2012 (8.0's Retail), 17 October 2013 (8.1's Retail)",
            "       Windows 8, the successor to Windows 7, was released generally on October 26, 2012. A number of significant changes were made on Windows 8, including the introduction of a user interface based around Microsoft's Metro design language with optimizations for touch-based devices such as tablets and all-in-one PCs. These changes include the Start screen, which uses large tiles that are more convenient for touch interactions and allow for the display of continually updated information, and a new class of apps which are designed primarily for use on touch-based devices. Other changes include increased integration with cloud services and other online platforms (such as social networks and Microsoft's own OneDrive (formerly SkyDrive) and Xbox Live services), the Windows Store service for software distribution, and a new variant known as Windows RT for use on devices that utilize the ARM architecture. An update to Windows 8, called Windows 8.1, was released on October 17, 2013, and includes features such as new live tile sizes, deeper OneDrive integration, and many other revisions. Windows 8 and Windows 8.1 have been subject to some criticism, such as removal of the Start menu.",
            "https://png2.cleanpng.com/sh/03c663887dc23354e6a8f7f4ca7b8512/L0KzQYi4UsIxN5Jrj5GAYUPkQYeCWcQzPmI5SpC8NUC2Q4e3UcE2OWQ8Tag8MUO1R4KCVsk1OV91htk=/5a3a1699426142.35033601151375631327196941.png",
            "Microsoft"
        ),

        /**
         * Windows 10
         */
        arrayOf(
            // group, name, version, codename, relDate, desc, logo, dev
            "Windows",
            "Windows 10",
            "10",
            "Threshold (until build 1511), Redstone (from build 1607)",
            "29 July 2015",
            "       Windows 10 is a series of personal computer operating systems produced by Microsoft as part of its Windows NT family of operating systems. It is the successor to Windows 8.1, and was released to manufacturing on July 15, 2015, and broadly released for retail sale on July 29, 2015. Windows 10 receives new builds on an ongoing basis, which are available at no additional cost to users, in addition to additional test builds of Windows 10 which are available to Windows Insiders. Devices in enterprise environments can receive these updates at a slower pace, or use long-term support milestones that only receive critical updates, such as security patches, over their ten-year lifespan of extended support.\n" +
                "\n" +
                "       One of Windows 10's most notable features is support for universal apps, an expansion of the Metro-style apps first introduced in Windows 8. Universal apps can be designed to run across multiple Microsoft product families with nearly identical code\u200D—\u200Cincluding PCs, tablets, smartphones, embedded systems, Xbox One, Surface Hub and Mixed Reality. The Windows user interface was revised to handle transitions between a mouse-oriented interface and a touchscreen-optimized interface based on available input devices\u200D—\u200Cparticularly on 2-in-1 PCs, both interfaces include an updated Start menu which incorporates elements of Windows 7's traditional Start menu with the tiles of Windows 8. Windows 10 also introduced the Microsoft Edge web browser, a virtual desktop system, a window and desktop management feature called Task View, support for fingerprint and face recognition login, new security features for enterprise environments, and DirectX 12.\n" +
                "\n" +
                "       Windows 10 received mostly positive reviews upon its original release in July 2015. Critics praised Microsoft's decision to provide a desktop-oriented interface in line with previous versions of Windows, contrasting the tablet-oriented approach of 8, although Windows 10's touch-oriented user interface mode was criticized for containing regressions upon the touch-oriented interface of Windows 8. Critics also praised the improvements to Windows 10's bundled software over Windows 8.1, Xbox Live integration, as well as the functionality and capabilities of the Cortana personal assistant and the replacement of Internet Explorer with Edge. However, media outlets have been critical of changes to operating system behaviors, including mandatory update installation, privacy concerns over data collection performed by the OS for Microsoft and its partners and the adware-like tactics used to promote the operating system on its release.\n" +
                "\n" +
                "       Although Microsoft's goal to have Windows 10 installed on over a billion devices within three years of its release had failed, it still had an estimated usage share of 59% of all the Windows versions on traditional PCs,[17] and thus 45% of traditional PCs were running Windows 10 by August 2019. Across all platforms (PC, mobile, tablet and console), 35% of devices run some kind of Windows, Windows 10 or older.",
            "https://www.pinclipart.com/picdir/middle/8-82688_microsoft-windows-windows-10-logo-transparent-clipart.png",
            "Microsoft"
        )
    )
    val listOfDetails: ArrayList<Windows>
        get() {
            val listOfData = arrayListOf<Windows>()
            for (data in datas) {
                val windows = Windows()
                windows.group = data[0] as String
                windows.name = data[1] as String
                windows.version = data[2] as String
                windows.codename = data[3] as String
                windows.relDate = data[4] as String
                windows.desc = data[5] as String
                windows.logo = data[6] as String
                windows.dev = data[7] as String
                listOfData.add(windows)
            }

            return listOfData
        }
}