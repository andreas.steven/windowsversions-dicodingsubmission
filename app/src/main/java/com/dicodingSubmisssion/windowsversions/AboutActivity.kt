/**
 * Class yang menampilkan data pembuat aplikasi
 */

package com.dicodingSubmisssion.windowsversions

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
    }


    // set isi menu "About Me" agar bisa ditampilkan dan diklik
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.back_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        goTo(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    private fun goTo(selectedMenu: Int){
        when (selectedMenu){
            R.id.intent_back -> {
                this.finish()
            }
        }
    }
}
