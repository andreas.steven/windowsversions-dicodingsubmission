/**
 * Class yang menampilkan splash screen / tampilan pembuka
 */

package com.dicodingSubmisssion.windowsversions

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()

        // intent di delay agar splash screen bisa terlihat
        Handler().postDelayed({
            var gotoActivity = Intent(this, MainActivity::class.java)
            startActivity(gotoActivity)
            finish()
        }, 550)
    }

}
