/**
 * Class untuk menampilkan info yang rinci tentang versi Windows yang telah dipilih di menu awal
 */

package com.dicodingSubmisssion.windowsversions

import android.R.attr.*
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_details.*
import java.util.ArrayList
import android.graphics.drawable.Drawable
import android.view.Display
import android.view.View
import android.view.ViewGroup.MarginLayoutParams

class DetailsActivity : AppCompatActivity() {

    // Deklarasi dan inisialisasi variabel
    private var TAG: String = "DetailsActivity : "
    private var title: String = "Details"
    private val windows = Windows()


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        showAll()
    }

    // Fungsi untuk me-return index array data
    private fun getPos(name: String) : Int{

        var counter = 0
        var listWindows = WindowsData.listOfDetails

        for (i in 0 until listWindows.size) {
            if (listWindows[i].name == name) {
                counter = i
                break
            }
        }
        listWindows = arrayListOf()
        return counter
    }

    // Fungsi untuk mengambil data url gambar
    private fun getPictsList(): ArrayList<String> {

        return WindowsPictData.listOfPictures[ getPos(this.windows.name) ]
    }

    // Menampilkan gambar - gambar untuk galeri gambar
    private fun showGallery(): LinearLayout {

        var layout: LinearLayout = findViewById(R.id.gallery)

        for (i in 0 until this.getPictsList().size) {
            val image = ImageView(this)

            // Set style gambar
            image.layoutParams = android.view.ViewGroup.LayoutParams(800, 500)
            image.setPadding(25,0,25,0)

            // Load gambar menggunakan Glide
            Glide.with(this)
                .load( getPictsList().get(i) )
                .into(image)

            // Tambah ImageView ke layout
            layout.addView(image)
        }
        return layout
    }


    // Fungsi untuk mencegah force close apabila data tidak dipass ke intent menggunakan if
    private fun showAll() {

        if( getIntent().hasExtra("group") ) {

            // Inisialisasi
            var idLogo = R.id.dImgLogo
            var idName = R.id.datName
            var idDev = R.id.datDev
            var idGroup = R.id.datGroup
            var idVersion = R.id.datVersion
            var idCodename = R.id.datCodename
            var idRelDate = R.id.datRelDate
            var showGallery: LinearLayout
            var idDesc = R.id.desc

            // Get data dari Intent (putExtra) dan tampung ke variable penampung
            this.windows.logo = getIntent().getStringExtra("logo").toString()
            this.windows.name = getIntent().getStringExtra("name").toString()
            this.windows.dev = getIntent().getStringExtra("dev").toString()
            this.windows.group = getIntent().getStringExtra("group").toString()
            this.windows.version = getIntent().getStringExtra("version").toString()
            this.windows.codename = getIntent().getStringExtra("codename").toString()
            this.windows.relDate = getIntent().getStringExtra("relDate").toString()
            this.windows.desc = getIntent().getStringExtra("desc").toString()

            // Ubah judul ActionBar
            (supportActionBar as ActionBar).title = this.title

            // Taruh data dari variabel penampung ke layout
            showId(idLogo, this.windows.logo, "imageview")
            showId(idName, this.windows.name, "textview")
            showId(idDev, this.windows.dev, "textview")
            showId(idGroup, this.windows.group, "textview")
            showId(idVersion, this.windows.version, "textview")
            showId(idCodename, this.windows.codename, "textview")
            showId(idRelDate, this.windows.relDate, "textview")
            showGallery = showGallery()
            showId(idDesc, this.windows.desc, "textview")
        }else{
            // Kembali ke menu awal jika tidak ada data yang dipassing
            this.finish()
        }
    }

    // Tampilkan view berdasarkan tipe layout dengan parameter id viewnya, isi view, dan tipe view
    private fun showId(id: Int, content: String, type: String) {

        if(type == "textview") {
            var txtView: TextView = findViewById(id)
            txtView.setText(content)
        } else if(type == "imageview") {
            var imgView: ImageView = findViewById(id)
            Glide.with(this)
                .load(content)
                .apply(RequestOptions().override(83, 83))
                .into(imgView)
        }
    }


    // set isi menu "Back" agar bisa ditampilkan dan diklik
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.back_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        goTo(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    private fun goTo(selectedMenu: Int){

        when (selectedMenu){
            R.id.intent_back -> {
                this.finish()
            }
        }
    }
}
