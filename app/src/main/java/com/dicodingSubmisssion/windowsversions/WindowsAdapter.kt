/**
 * Class Adapter untuk ambil data detil untuk setiap versi - versi Windows
 */

package com.dicodingSubmisssion.windowsversions

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import android.widget.Toast.makeText
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

class WindowsAdapter (val listWindows: ArrayList<Windows>) : RecyclerView.Adapter<WindowsAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {

        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_container, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {

        val (group,
            name,
            version,
            codename,
            relDate,
            desc,
            logo,
            dev) = listWindows[position]

        holder.viewholder_Group.text = group
        holder.viewholder_Version.text = version
        holder.viewholder_Codename.text = codename
        holder.viewholder_relDate.text = relDate
        Glide.with(holder.itemView.context)
            .load(logo)
            .transform(RoundedCorners(4))
            .apply( RequestOptions().override(83, 83) )
            .into(holder.viewholder_logo)

        holder.itemView.setOnClickListener{
            var intent = Intent(holder.itemView.context, DetailsActivity::class.java)
            intent.putExtra("group", group)
            intent.putExtra("name", name)
            intent.putExtra("version", version)
            intent.putExtra("codename", codename)
            intent.putExtra("relDate", relDate)
            intent.putExtra("desc", desc)
            intent.putExtra("logo", logo)
            intent.putExtra("dev", dev)
            holder.itemView.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {

        return listWindows.size
    }

    private fun getPos(name: String) : Int{

        var counter = 0
        for (i in 0 until listWindows.size) {
            if (listWindows[i].name == name) {
                counter = i
                break
            }
        }
        return counter
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var viewholder_Group: TextView = itemView.findViewById(R.id.group)
        var viewholder_Version: TextView = itemView.findViewById(R.id.version)
        var viewholder_Codename: TextView = itemView.findViewById(R.id.codename)
        var viewholder_relDate: TextView = itemView.findViewById(R.id.relDate)
        var viewholder_logo: ImageView = itemView.findViewById(R.id.imgLogo)
    }
}
