/**
 * Class yang befungsi sebagai model
 */

package com.dicodingSubmisssion.windowsversions

data class Windows(
    var group: String = "",
    var name: String = "",
    var version: String = "",
    var codename: String = "",
    var relDate: String = "",
    var desc: String = "",
    var logo: String = "",
    var dev: String = ""
)